###############################################
##    Elastycloud build instructions file    ##
###############################################
#
# Read about it here:
# https://docs.elastycloud.io/articles/elastycloud-build-yaml.html
#

# This section declares the package/project in this repo
# This info is matched against Console during the build
package:

  # the package-key registered in Console
  key: your-package-key

  # What is the type of this repo?
  #
  # Possible values: (string)
  # - project -> will be used only for one customer/site
  # - package -> will be used for multiple unrelated customers
  type: project

# This section declares how this package should be built
build:

  # Specify the version of the server environment you wish to use
  # This primarily specify the php-version that is running, but effect the whole
  # environment, including services like nginx, Varnish & Redis
  #
  # Note: You can specify a specific version to use, but it is highly recommended
  #       to use a "latest" version. Otherwise you will have to update this
  #       regularly to get new security patches in your services.
  #
  # Read more, and find a list and description of all tags here:
  # https://docs.elastycloud.io/articles/environment-versions.html
  #
  # type: string (default php-7.2_latest)
  environment_version: php-8.1_latest

  # Should we run 'composer update' during build?
  #
  # NOTE: this will use the latest packages according to the minimum
  # stability flag in the project, on every build. Meaning that you might
  # get different versions in the build compared to what's committed in
  # composer.lock
  #
  # This behavior is DESIRED on custom shops that won't have much development
  # done over time, meaning that the store will still get updates.
  #
  # This is however UNDESIRED when the project have constant development and
  # updates are installed manually.
  #
  # type: boolean (default: no)
  auto_update: no

  # List of locales to deploy
  #
  # NOTE: Has to be magento-compatible codes
  # The latest translation will be downloaded from eValingo during build for
  # the listed locales
  #
  # type: string[]
  locales:
  # - da_DK
  # - de_DE
    - en_US
  # - es_ES
  # - fi_FI
  # - fr_FR
  # - is_IS
  # - it_IT
  # - nb_NO
  # - nl_NL
    - sv_SE

  # List of composer repository authentications this project needs
  # Common use includes overriding the keys used for repo.magento.com to gain
  # access to modules purchased on Magento Marketplace
  #
  # Any hostname can be used as key if other third party repositories is needed
  #
  # Note: This section is optional and should only be used when needed.
  #       When activating this, just remove the # in front of each line, there
  #       should be exactly two spaces in front of "repositories:"
  #
  #repositories:
  #  repo.magento.com:
  #    username: uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu
  #    password: pppppppppppppppppppppppppppppppp
  #  example.com:
  #    username: some-user
  #    password: some-secret-password

# This section declares options that define your stores runtime
#
# These settings will be active during the build phase, and be forced in your
# database after deployment.
runtime:

  # Static Asset Management settings
  # Merge tends to block page render times and give above content fold warnings
  # in Google Pagespeed. Minify is highly recommended to decrease transfer size.
  #
  # type: int (1 or 0)
  javascript_minify: 1  # default: 1
  javascript_bundle: 1  # default: 1
  javascript_merge: 0   # default: 0
  css_minify: 1         # default: 1
  css_merge: 0          # default: 0
  html_minify: 1        # default: 1
  static_sign: 1        # default: 1
