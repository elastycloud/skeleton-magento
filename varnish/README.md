# Custom varnish VCL includes

Here you can specify VCL entries that will automatically be included during the build of your magento deployment.

## GEO IP

Elastycloud offers built in GEOIP support where varnish redirects the customer to the desired website automatically without touching the Magento2 backend first.

Since this happens without knowledge of what is configured in magento2. 
So you have to specify what domains/country codes should be redirected to and from.

### Get started with GEO IP

1. Rename geoip.vcl.sample to => geoip.vcl  
2. Replace the sample domains with yours
3. Specify the correct countrycodes in uppercase ex: SE or DK etc
4. Add them to version control and tag your commit
5. Test and Deploy as usual
6. Profit!

## Other custom VCL code injection

1. Rename the sample file that corresponds to the VCL section you want to modify. ex:
   recv_footer.vcl.sample => recv_footer.vcl
2. This particular file will be included in its entirety to the very end of the recv section of the VCL.
3. Add it to version control and tag your commit
4. Test and Deploy as usual
5. Profit!